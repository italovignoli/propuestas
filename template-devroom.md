# Title of the proposal

Brief introduction and motivation of the same.

## Proposal format

Choose one of these:

* [ ] Talk (25 minutes)
* [ ] Lightning talk (10 minutes)

## Description

Short (couple of paragraphs) description about what the talk is about.

## Target audience

Who Should Attend? 

## Speaker(s)

Who is going to give the talk? What do you/they do? (brief bio about yourself) What talks have you
given before?

## Observations

Any other relevant observations.

## Conditions

* [ ] I agree to follow the [code of conduct](https://eslib.re/2019/conducta/) and request this acceptance from the attendees and speakers.
* [ ] At least one person among those proposing will be present on the day scheduled for the talk.
